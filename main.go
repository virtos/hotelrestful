package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Hotel struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Region      string `json:"region"`
	Address     string `json:"address"`
	Desc        string `json:"description"`
	Rooms       Rooms  `json:"rooms"`
	Coordinates string `json:"coordinates"`
}
type HotelId struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
type Room struct {
	Name      string `json:"Тип номера"`
	Capacity  int    `json:"Вмещает"`
	Available int    `json:"Available"`
}
type Hotels []Hotel
type Rooms []Room
type HotelIds []HotelId

var HotelData Hotels

func populateData() {
	HotelData = Hotels{
		Hotel{ID: 6, Name: "Гостиничный комплекс Вертикаль", Coordinates: "58.6349773406982,53.8077280023929", Rooms: Rooms{Room{Name: "Номер-студио", Capacity: 2, Available: 8}, Room{Name: "Люкс с сауной", Capacity: 2, Available: 2}}, Region: "Абзаково", Address: "Новоабзаково, улица горнолыжная 29/2, Абзаково, Россия", Desc: " Комплекс «Вертикаль» находится в 800 метрах от горнолыжного подъемника на курорте Ново-Абзаково. К услугам гостей открытый бассейн. В радиусе 500 метров расположены кафе и магазины."},
		Hotel{ID: 2, Name: "Гостиничный комплекс Тау-Таш", Coordinates: "58.63577127,53.80680303", Rooms: Rooms{Room{Name: "Номер-студио", Capacity: 2, Available: 8}, Room{Name: "Люкс с сауной", Capacity: 2, Available: 2}}, Region: "Абзаково", Address: "Горнолыжная Улица 33, Абзаково, Россия", Desc: "К услугам гостей этого гостиничного комплекса на горнолыжном курорте Ново-Абзаково – аквапарк с крытым бассейном, финская сауна и боулинг."},
		Hotel{ID: 3, Name: "Гостиница Усадьба Еловое", Coordinates: "58.6417391896248,53.8107641058551", Rooms: Rooms{Room{Name: "Номер-студио", Capacity: 2, Available: 8}, Room{Name: "Люкс с сауной", Capacity: 2, Available: 2}}, Region: "Абзаково", Address: "Переулок Ёлочка 3, Абзаково, Россия", Desc: "Отель \"Усадьба Еловое\" расположен в сосновом бору на территории популярного лыжного курорта Новоабзаково на Южном Урале в Республике Башкортостан. "},
		Hotel{ID: 5, Name: "Отель Горный Воздух", Coordinates: "58.6371660232544,53.8102493933091", Rooms: Rooms{Room{Name: "Номер-студио", Capacity: 2, Available: 8}, Room{Name: "Люкс с сауной", Capacity: 2, Available: 2}}, Region: "Абзаково", Address: "Улица Горнолыжная 4, Абзаково, Россия", Desc: "Отель «Горный Воздух» с принадлежностями для барбекю и террасой расположен в селе Абзаково. К услугам гостей бесплатный Wi-Fi и круглосуточная стойка регистрации. В отеле работает лыжная школа."},
		Hotel{ID: 1, Name: "Отель Смайл", Coordinates: "58.59884262,53.83103084", Rooms: Rooms{Room{Name: "Номер-студио", Capacity: 2, Available: 8}, Room{Name: "Люкс с сауной", Capacity: 2, Available: 2}}, Region: "Абзаково", Address: "Улица Школьная 44, Абзаково, Россия", Desc: "Отель «Улыбка» находится в 5 км от горнолыжного курорта Ново-Абзаково. К услугам гостей кафе, принадлежности для барбекю и бесплатный Wi-Fi."},
		Hotel{ID: 4, Name: "Отель Яш-Тан", Coordinates: "58.6371123790741,53.8060538544256", Rooms: Rooms{Room{Name: "Номер-студио", Capacity: 2, Available: 8}, Room{Name: "Люкс с сауной", Capacity: 2, Available: 2}}, Region: "Абзаково", Address: "улица Горнолыжная 34, Абзаково, Россия", Desc: "Отель «Яш-Тан» с видом на горы и принадлежностями для барбекю расположен на горнолыжном курорте Абзаково в Башкортостане, в 48 км от Магнитогорска."},
		Hotel{ID: 0, Name: "Эдельвейс", Coordinates: "58.6361575126648,53.8111046045274", Rooms: Rooms{Room{Name: "Номер-студио", Capacity: 2, Available: 8}, Room{Name: "Люкс с сауной", Capacity: 2, Available: 2}}, Region: "Абзаково", Address: "Горный проезд 1A, Абзаково, Россия", Desc: "Гостиничный комплекс \"Эдельвейс\" расположен на территории лыжного курорта Абзаково, в 1 км от горнолыжного центра."},
	}
}

func main() {
	populateData()
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", Index)
	router.HandleFunc("/hotels", ReturnAllHotels)
	router.HandleFunc("/hotels/{name}", FindHotels)
	router.HandleFunc("/hotel/{id}", HotelDetails)
	router.HandleFunc("/geo/{coord}/{distance}", FindByDistance)
	log.Fatal(http.ListenAndServe(":8080", router))
}
