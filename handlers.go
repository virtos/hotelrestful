package main

import (
	"encoding/json"
	"fmt"
	"html"
	"log"
	"math"
	"net/http"
	"sort"
	"strconv"
	"strings"

	"github.com/gen1us2k/go-translit"
	"github.com/gorilla/mux"
)

func ReturnAllHotels(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(HotelData)
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Test RESTful API app, %q", html.EscapeString(r.URL.Path))
}

func HotelDetails(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	hotelID, _ := strconv.Atoi(vars["id"])
	found := false
	for _, ch := range HotelData {
		if ch.ID == hotelID {
			json.NewEncoder(w).Encode(ch)
			found = true
			break
		}
		if !found {
			fmt.Fprintf(w, "ничего не найдено")
		}
	}
}

func FindHotels(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	sn := translit.Translit(strings.ToLower(vars["name"]))
	var m HotelIds
	for _, ch := range HotelData {
		if strings.Contains(strings.ToLower(ch.Name), sn) || strings.Contains(strings.ToLower(ch.Region), sn) {
			m = append(m, HotelId{ch.ID, ch.Name})
		}
	}
	if len(m) > 0 {
		json.NewEncoder(w).Encode(m)
	} else {
		fmt.Fprintf(w, "ничего не найдено")
	}
}

func FindByDistance(w http.ResponseWriter, r *http.Request) {
	res := make(map[float64]HotelId)
	vars := mux.Vars(r)
	coord := strings.Split(vars["coord"], ",")
	if len(coord) != 2 {
		log.Printf("Wrong coordinates supplied: %s", vars["coord"])
		fmt.Fprintf(w, "ничего не найдено")
		return
	}

	lat1, _ := strconv.ParseFloat(coord[0], 64)
	lon1, _ := strconv.ParseFloat(coord[1], 64)
	dist, _ := strconv.ParseFloat(vars["distance"], 64)

	for _, ch := range HotelData {
		coord2 := strings.Split(ch.Coordinates, ",")
		if len(coord2) == 2 {
			lat2, _ := strconv.ParseFloat(coord2[0], 64)
			lon2, _ := strconv.ParseFloat(coord2[1], 64)
			if d := distance(lat1, lon1, lat2, lon2); d < dist {
				res[d] = HotelId{ch.ID, ch.Name}
			}
		}
	}
	if len(res) > 0 {
		var keys []float64
		for k := range res {
			keys = append(keys, k)
		}
		sort.Float64s(keys)
		for _, k := range keys {
			json.NewEncoder(w).Encode(res[k])
		}
	} else {
		fmt.Fprintf(w, "ничего не найдено")
	}
}

// geo calc courtesy of cdipaolo
func hsin(theta float64) float64 {
	return math.Pow(math.Sin(theta/2), 2)
}
func distance(lat1, lon1, lat2, lon2 float64) float64 {
	// convert to radians
	// must cast radius as float to multiply later
	var la1, lo1, la2, lo2, r float64
	la1 = lat1 * math.Pi / 180
	lo1 = lon1 * math.Pi / 180
	la2 = lat2 * math.Pi / 180
	lo2 = lon2 * math.Pi / 180

	r = 6378100 // Earth radius in METERS

	h := hsin(la2-la1) + math.Cos(la1)*math.Cos(la2)*hsin(lo2-lo1)
	return 2 * r * math.Asin(math.Sqrt(h))
}
